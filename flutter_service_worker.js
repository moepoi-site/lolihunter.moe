'use strict';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  ".git/COMMIT_EDITMSG": "ec4d59b2732f2f153240a8ff746282a6",
".git/config": "280b4d7fb50bafc8542a70a838252bb7",
".git/description": "a0a7c3fff21f2aea3cfa1d0316dd816c",
".git/FETCH_HEAD": "2ef2449dc130eddbc9604325383bff8e",
".git/HEAD": "4cf2d64e44205fe628ddd534e1151b58",
".git/hooks/applypatch-msg.sample": "ce562e08d8098926a3862fc6e7905199",
".git/hooks/commit-msg.sample": "579a3c1e12a1e74a98169175fb913012",
".git/hooks/fsmonitor-watchman.sample": "ecbb0cb5ffb7d773cd5b2407b210cc3b",
".git/hooks/post-update.sample": "2b7ea5cee3c49ff53d41e00785eb974c",
".git/hooks/pre-applypatch.sample": "054f9ffb8bfe04a599751cc757226dda",
".git/hooks/pre-commit.sample": "e4db8c12ee125a8a085907b757359ef0",
".git/hooks/pre-push.sample": "3c5989301dd4b949dfa1f43738a22819",
".git/hooks/pre-rebase.sample": "56e45f2bcbc8226d2b4200f7c46371bf",
".git/hooks/pre-receive.sample": "2ad18ec82c20af7b5926ed9cea6aeedd",
".git/hooks/prepare-commit-msg.sample": "2b5c047bdb474555e1787db32b2d2fc5",
".git/hooks/update.sample": "517f14b9239689dff8bda3022ebd9004",
".git/index": "890ea200aa8c8a93afc7c7fff9a49b75",
".git/info/exclude": "036208b4a1ab4a235d75c181e685e5a3",
".git/logs/HEAD": "3cfb879e93936ef158c9e38141b08cc3",
".git/logs/refs/heads/master": "3cfb879e93936ef158c9e38141b08cc3",
".git/logs/refs/remotes/origin/HEAD": "84a1c8d1e80902334a56a03da74582e9",
".git/logs/refs/remotes/origin/master": "a3deba098dd6068e241f55664b7f66ac",
".git/objects/06/d3c8f6e477498e0e0e96a1f57ba56d67fbc15c": "8ce15026e9e6ce0db69c1ac3a0053f8d",
".git/objects/0f/816fb5068fb5d0dc1623718a94d7a34c5edfe4": "48392ce692d6328aef69a753fa305233",
".git/objects/13/cd698afa86edb334bec59991cd113491a5a4f1": "01d69635e49925db17e7c32e26fc974f",
".git/objects/15/9b15011009e1e691b0761e5627dc3f3aa452ad": "822c0ced8e90203ed87ff7e0082d7945",
".git/objects/20/5bb5db271c6d8de8399864c7bb9b917f638893": "c993b22f115d7f3ae6d5b7b212806539",
".git/objects/2a/780c6e2ade05573b54d24a9ccf0183e7b79bd0": "ef25024df86decf5fc8d2939743a5d6b",
".git/objects/34/d95926424d1cd4ffb66c00ea912846a9d21465": "f7a9b7314771553869084cf84c82b090",
".git/objects/3b/7653dd94decebbea88eddb7cd8260ed0d45eb1": "084c420d08a6716adc9f73d1246cd789",
".git/objects/41/a4dd469361683a273787456af2b0ea02098824": "857a076159c9e84bdcfedfde92861631",
".git/objects/47/61f2d0c240972dcd71209df52108d25db847df": "924c72e3e213390ddfcc4c731f20e4d0",
".git/objects/47/6ccf3e778f5b1b07adf5584f93a7dea53e3c17": "e0a3b0a5427d968fe6d5bed45fb105e3",
".git/objects/47/cafbfff3a0773d843efdf277697adc77794b09": "fa9b782702dfa051d3624c2bfd2b550e",
".git/objects/49/cf667db7f58e9a82ca4f2e630e7871ee0dea94": "23c5ea52c29852d039487ed786e667cb",
".git/objects/4f/3868336ff97c53eec1a15539955fdde8f83388": "70dc05efaebccdd4045c30d59f4cd199",
".git/objects/56/0c9242e2b864fa159b349796e1d022a120efbf": "19f19f4df2825b7c6b887b7f8e322a98",
".git/objects/56/e6a080569882b449dc611beb41f9262b888fcf": "6c491fa6adfb624aa909dca12e139a1e",
".git/objects/5a/597aa0a4a7b36ca63c9c20b2127184bfd5f085": "853cb349674b1de2a316e902df1db8f1",
".git/objects/65/0389aa512c7fd3f6b14c14a594a6d02e030b35": "d4ea9b7f8e25d440ac0e6a90bae233ae",
".git/objects/68/47ba3d5d5da9201f3c4b655a28deee5bd9bfe4": "6165731846e91a48207cc6ce08c137de",
".git/objects/68/fbe8cccafd875415b394b1e96b55fe8c0a0fd4": "395f26f4c61c51c598585d6008b94046",
".git/objects/6f/b9cc4fbab472159729f2b64ea83325ae66977a": "bcdad6fff816cb4b0709c4d719d51384",
".git/objects/70/5400a6f991b612f358bc710b5511c61cf56e97": "4ed317e526ce9d66b79cfe0d0e479868",
".git/objects/76/735825ccd2272e3f8b5d5da2987475a51f7156": "3ac3b02203063fedd4f69ba4c40cdce9",
".git/objects/79/f6824a116619fc7e6b664ce01c69a6c66afafa": "f9c027b03eecbfcb56356078c6375b28",
".git/objects/7a/cfa8147deb7f3f0dc5b61f4c421d7202da91ca": "3dbd2e8637d20ec579e94de3e341f709",
".git/objects/7b/ac2545ff08c4681aa0519291dfe38d693affd4": "b98efbc1eaf906fecd436855c0ae9273",
".git/objects/81/0337fcab9374ea7916511a5b9b59c1fe38c5fd": "cc99e87ef5a5ad26f76eb93e555d98fa",
".git/objects/88/cfd48dff1169879ba46840804b412fe02fefd6": "e42aaae6a4cbfbc9f6326f1fa9e3380c",
".git/objects/8a/aa46ac1ae21512746f852a42ba87e4165dfdd1": "1d8820d345e38b30de033aa4b5a23e7b",
".git/objects/91/98e16c33ad9a583bfe24e1739ecb272f5131ac": "9210d62eed31a7aeb3303819ba621cbd",
".git/objects/92/bbc202d58b8f173c0395ae9c0d01a04e4e5a21": "f134c838d3e267ef424054ef2188d18f",
".git/objects/95/19e1d75e8e60fc461d42dceff7162076484747": "87166efde232eca9c4f1ad4118b6d046",
".git/objects/95/e2353d26a17551f76085319d20ee6a507c93f0": "8e361fcc64e9c5cc367ce8553c5fdca2",
".git/objects/98/a77eeb810c260f96c25c10ef2ebabdc19666a9": "ec243d2e7787d4e3c399611804ba25c7",
".git/objects/a2/cbaed1ee9b293669c0de398ca4f991028ffbe7": "cb8a895e762603b53c43c8a1a31bb80f",
".git/objects/ab/6d61f959c28f5f9b42c1b0d449568decdf6e94": "e9cdd5bf3fb7ee34e1135b8f9244b9e8",
".git/objects/ae/13e4a0e8c0499016c9b4a4add6de9bc453026e": "14383d3e0845944b8fb5f55de714eacf",
".git/objects/b3/7752f586c624368bebb79c285749c6042cf76f": "3eca59a0851ca2442a58360163f0ef82",
".git/objects/b7/49bfef07473333cf1dd31e9eed89862a5d52aa": "36b4020dca303986cad10924774fb5dc",
".git/objects/bd/3457a536694677e5a7cdb6fc6fae662a44cd2f": "464ff58417507ac4edba6c2cf8806bbb",
".git/objects/c2/3ce3f70ce72326f4c35872dd28085729dfba60": "16faf3c15dc43969986234ffaeece286",
".git/objects/c4/d99730ee3abc4321a3c5ff3444941cc76e4a48": "f243e0d77d540704b8b1d4d0864dd2df",
".git/objects/c7/fb843093bd79fa1b23a92e85314eb3c4592353": "31af61ee2b6a75068301c80aa1d9f941",
".git/objects/d8/7964a6fa88f2b5a9af4c42fd187424acf00d6f": "3b145ab1b96688748e950163c8f535bc",
".git/objects/d9/b6c34a8ea8c6465bf272626da701e6cb9c07e0": "1f57be80fbb14104f5e747eeb8e618cf",
".git/objects/dd/56eabbf298449db91bfa8644c6fbb20b8e6dda": "3fa6bb294070401dbba0b01792b84ce0",
".git/objects/e1/1909a923fee9f950d2f8c742f7439836815f9e": "fd0dbc431a08973f876c18307d799e06",
".git/objects/e2/d982fac569584322ccd65e2bbbb5f6aea32f87": "79f56adb98768ac4f2cebea03b2f42ca",
".git/objects/ea/9e7cfd3d57db4e00a8a73c00cf029ddbca117d": "b123fc523533f131261e044b31441068",
".git/objects/ec/0e43d800821ea71e680f3aece7aeab440436fd": "ecc862ed44ede4f9221365f002384144",
".git/objects/f3/d3a9803d3591d92e9b295ab82d3bdfc7078f8a": "35b064de979092212493892f49dd8e1d",
".git/objects/f8/9d2017f25e0a9b2cd0c636bba78acb9a8f05d4": "4f8f8b04943c8409d97a2a600ad010ff",
".git/objects/fc/565ec170b7fd55cbf422b8188a6f64eddda60e": "c1d5b622c8e63d1865785872c9d4a21d",
".git/ORIG_HEAD": "f6ed7296c1edf1bd10a5217058d1dcbd",
".git/packed-refs": "01935ec60a30a82f3a895258c606c935",
".git/refs/heads/master": "3aa237e5aec00383785043ae86b113b4",
".git/refs/remotes/origin/HEAD": "73a00957034783b7b5c8294c54cd3e12",
".git/refs/remotes/origin/master": "3aa237e5aec00383785043ae86b113b4",
"assets/AssetManifest.json": "de3de56a2195e4597b390630317e86ef",
"assets/assets/images/EXTjYCtUwAA_cBO.jpg": "f9ed950cba3273181070eac04fd30571",
"assets/FontManifest.json": "01700ba55b08a6141f33e168c4a6c22f",
"assets/fonts/MaterialIcons-Regular.ttf": "56d3ffdef7a25659eab6a68a3fbfaf16",
"assets/LICENSE": "4c7aeac6acf1b0a3d93ed0d8e6c3b5ab",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "115e937bb829a890521f72d2e664b632",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"index.html": "eee7fb255f8cf4794c88137acbfc423d",
"/": "eee7fb255f8cf4794c88137acbfc423d",
"LICENSE": "2455d6c6c87eb16101c53cf129365747",
"main.dart.js": "bce1d055f9200420531e91d5b57193f7",
"manifest.json": "fffb28ac2674fa6c4306794160364678"
};

self.addEventListener('activate', function (event) {
  event.waitUntil(
    caches.keys().then(function (cacheName) {
      return caches.delete(cacheName);
    }).then(function (_) {
      return caches.open(CACHE_NAME);
    }).then(function (cache) {
      return cache.addAll(Object.keys(RESOURCES));
    })
  );
});

self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request)
      .then(function (response) {
        if (response) {
          return response;
        }
        return fetch(event.request);
      })
  );
});
